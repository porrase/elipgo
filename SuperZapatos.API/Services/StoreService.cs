﻿using SuperZapatos.Business;
using SuperZapatos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatos.API.Services
{
    public interface IStoreService
    {
        object GetAll();
        object Add(StoreEntity se);
        object Update(StoreEntity se);
        object Delete(string id);

    }
    public class StoreService : IStoreService
    {
        StoreBusiness oStoreBusiness = new StoreBusiness();

        public object GetAll()
        {
            return oStoreBusiness.GetAll();
        }
        public object Add(StoreEntity se)
        {
            return oStoreBusiness.Add(se);
        }
        public object Update(StoreEntity se)
        {
            return oStoreBusiness.Update(se);
        }
        public object Delete(string id)
        {
            return oStoreBusiness.Delete(id);
        }
    }
}