﻿using SuperZapatos.Business;
using SuperZapatos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatos.API.Services
{
    public interface IArticleService : IServiceBase<ArticleEntity>
    {
        object GetAll();
        object GetArticleByStoreId(string id);       
    }

    public class ArticleService : IArticleService
    {
        ArticleBusiness oArticleBusiness = new ArticleBusiness();

        public object Add(ArticleEntity ae)
        {
            return oArticleBusiness.Add(ae);
        }

        public object Delete(string id)
        {
            return oArticleBusiness.Delete(id);
        }

        public object GetAll()
        {
            return oArticleBusiness.GetAll();
        }

        public object GetArticleByStoreId(string id)
        {
            return oArticleBusiness.GetArticleByStoreId(id);
        }

        public object Update(ArticleEntity ae)
        {
            return oArticleBusiness.Update(ae);
        }
    }
}