﻿using SuperZapatos.Business;
using SuperZapatos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SuperZapatos.API.Services
{
    public interface IServiceBase<T>
    {
        object GetAll();       
        object Add(T ae);
        object Update(T ae);
        object Delete(string id);
    } 
}