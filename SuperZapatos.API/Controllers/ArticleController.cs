﻿using SuperZapato.Entity.API;
using SuperZapatos.API.Services;
using SuperZapatos.Business;
using SuperZapatos.Entity;
using System;
using System.Net.Http;
using System.Web.Http;

namespace SuperZapatos.API.Controllers
{
    //[Authorize] TODO:Extension para permitir algun modelo de autorizacion. 
    [System.Web.Http.Route("services/articles")]
    public class ArticleController : ApiController
    {
        IArticleService articleService = new ArticleService();

        public object GetAll()
        {
            try
            {               
                return Ok(articleService.GetAll());
            }
            catch (Exception e)
            {
                //TODO:Log de exception
                return InternalServerError(e);
            }
        }

        [System.Web.Http.Route("services/articles/stores/{id}")]
        public object GetArticleByStoreId(string id)
        {
            try
            {
                var a = articleService.GetArticleByStoreId(id);

                if (a is ResponseError)
                {
                    return Request.CreateResponse(((ResponseError)a).error_code, a);
                }

                return Ok(a);
            }
            catch (Exception e)
            {
                //TODO:Log de exception
                return InternalServerError(e);
            }
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("services/article")]
        public object Add(ArticleEntity ae)
        {
            try
            {               
                var a = articleService.Add(ae);

                if (a is ResponseError)
                {
                    return Request.CreateResponse(((ResponseError)a).error_code, a);
                }

                return Ok(a);
            }
            catch (Exception e)
            {
                //TODO:Log de exception
                return InternalServerError(e);                
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("services/article")]
        public object Update(ArticleEntity ae)
        {
            try
            {               
                var a = articleService.Update(ae);

                if (a is ResponseError)
                {
                    return Request.CreateResponse(((ResponseError)a).error_code, a);
                }

                return Ok(a);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("services/article/{id}")]
        public object Delete(string id)
        {
            try
            {               
                var a = articleService.Delete(id);

                if (a is ResponseError)
                {
                    return Request.CreateResponse(((ResponseError)a).error_code, a);
                }

                return Ok(a);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
