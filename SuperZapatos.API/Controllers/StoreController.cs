﻿using SuperZapato.Entity;
using SuperZapato.Entity.API;
using SuperZapatos.API.Services;
using SuperZapatos.Business;
using SuperZapatos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SuperZapatos.API.Controllers
{
    //[Authorize] TODO:Extension para permitir algun modelo de autorizacion. 
    [System.Web.Http.Route("services/stores")]
    public class StoreController : ApiController
    {
        IStoreService storeService = new StoreService();

        public object GetAll()
        {
            try
            {               
                return Ok(storeService.GetAll());
            }
            catch (Exception e)
            {
                //TODO:Log de exception
                return InternalServerError(e);
            }
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("services/store")]
        public object Add(StoreEntity se)
        {
            try
            {               
                var a = storeService.Add(se);

                if (a is ResponseError)
                {
                    return Request.CreateResponse(((ResponseError)a).error_code, a);
                }

                return Ok(a);
            }
            catch (Exception e)
            {
                //TODO:Log de exception
                return InternalServerError(e);
            }
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("services/store")]
        public object Update(StoreEntity ae)
        {
            try
            {               
                var a = storeService.Update(ae);

                if (a is ResponseError)
                {
                    return Request.CreateResponse(((ResponseError)a).error_code, a);
                }

                return Ok(a);
            }
            catch (Exception e)
            {
                //TODO:Log de exception
                return InternalServerError(e);
            }
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("services/store/{id}")]
        public object Delete(string id)
        {
            try
            {               
                var a = storeService.Delete(id);

                if (a is ResponseError)
                {
                    return Request.CreateResponse(((ResponseError)a).error_code, a);
                }

                return Ok(a);
            }
            catch (Exception e)
            {
                //TODO:Log de exception
                return InternalServerError(e);
            }
        }
    }
}
