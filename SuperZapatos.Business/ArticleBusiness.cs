﻿using SuperZapato.Entity;
using SuperZapato.Entity.API;
using SuperZapatos.Data.Model;
using SuperZapatos.Data.Repositories;
using SuperZapatos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Business
{
    public class ArticleBusiness
    {
        private ArticleRepository repository = null;

        public ArticleBusiness()
        {
            repository = new ArticleRepository();
        }

        public object GetAll()
        {
            var list = repository.GetAll();

            if (list != null)
            {
                return new
                {
                    articles = list,
                    success = true,
                    total_elements = list.Count()
                };
            }
            else
            {
                return new
                {
                    errors = string.Empty
                };
            }
        }

        public object GetArticleByStoreId(string id)
        {
            int _id = 0;

            if(!int.TryParse(id, out _id))
                return new ResponseError
                {
                    error_code = HttpStatusCode.BadRequest,
                    error_msg = "Bad Request"
                };

            StoreRepository sr = new StoreRepository();

            var s = sr.FindBy(i => i.Id.Equals(_id)).SingleOrDefault();

            if (s == null)
                return new ResponseError
                {
                    error_code = HttpStatusCode.NotFound,
                    error_msg = "Record not found"
                };
                
            var a = repository.GetArticleById(_id);

            if (a != null)
            {
                return new
                {
                    articles = a,
                    success = true,
                    total_elements = a.Count()
                };
            }
            else
            {
                return new
                {
                    errors = string.Empty
                };
            }
        }

        public object Add(ArticleEntity ae)
        {
            if (ae == null)
                return new ResponseError
                {
                    error_code = HttpStatusCode.BadRequest,
                    error_msg = "Bad Request"
                };

            StoreRepository sr = new StoreRepository();

            var s = sr.FindBy(i => i.Name.Equals(ae.Store_Name)).SingleOrDefault();

            if (s == null)
                return new ResponseError
                {
                    error_code = HttpStatusCode.NotFound,
                    error_msg = "Store not found"
                };

            Article a = new Article();

            a.Name = ae.Name;
            a.Price = ae.Price;
            a.total_in_shelf = ae.Total_In_Shelf;
            a.total_in_vault = ae.Total_In_Vault;
            a.Store = new Store() { Id = s.Id, Name = s.Name, Address = s.Address };

            repository.Add(a);
            repository.Save();

            return true;
        }

        public object Update(ArticleEntity ae)
        {
            if (ae.Id != null)
                return new ResponseError
                {
                    error_code = HttpStatusCode.BadRequest,
                    error_msg = "Bad Request"
                };

            var s = repository.FindBy(i => i.Id.Equals(ae.Id)).SingleOrDefault();

            if (s == null)
                return new ResponseError
                {
                    error_code = HttpStatusCode.NotFound,
                    error_msg = "Record not found"
                };

            s.Name = ae.Name;
            s.Price = ae.Price;
            s.total_in_shelf = ae.Total_In_Shelf;
            s.total_in_vault = ae.Total_In_Vault;
            s.Store = new StoreRepository().FindBy(st => st.Name.Equals(ae.Store_Name)).SingleOrDefault();

            repository.Update(s);
            repository.Save();

            return true;
        }

        public object Delete(string id)
        {
            int _id = 0;

            if (!int.TryParse(id, out _id))
                return new ResponseError
                {
                    error_code = HttpStatusCode.BadRequest,
                    error_msg = "Bad Request"
                };

            var s = repository.FindBy(i => i.Id.Equals(_id)).SingleOrDefault();

            if (s == null)
                return new ResponseError
                {
                    error_code = HttpStatusCode.NotFound,
                    error_msg = "Record not found"
                };

            repository.Delete(s);
            repository.Save();

            return true;
        }
    }
}
