﻿using SuperZapato.Entity;
using SuperZapato.Entity.API;
using SuperZapatos.Data.Repositories;
using SuperZapatos.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Business
{
    public class StoreBusiness
    {
        private StoreRepository repository = null;

        public StoreBusiness()
        {
            repository = new StoreRepository();
        }

        public object GetAll()
        {
            var list = repository.GetAll();

            if (list != null)
            {
                return new
                {
                    stores = list,
                    success = true,
                    total_elements = list.Count()
                };
            }
            else
            {
                return new
                {
                    errors = string.Empty
                };
            }
        }

        public object Add(StoreEntity se)
        {
            if (se == null)
                return new ResponseError
                {
                    error_code = HttpStatusCode.BadRequest,
                    error_msg = "Bad Request"
                };

            repository.Add(new Data.Model.Store()
            {
                Name = se.Name,
                Address = se.Address
            });
            repository.Save();

            return true;
        }

        public object Update(StoreEntity se)
        {
            if (se.Id != null)
                return new ResponseError
                {
                    error_code = HttpStatusCode.BadRequest,
                    error_msg = "Bad Request"
                };

            var s = repository.FindBy(i => i.Id.Equals(se.Id)).SingleOrDefault();

            if (s == null)
                return new ResponseError
                {
                    error_code = HttpStatusCode.NotFound,
                    error_msg = "Record not found"
                };

            s.Name = se.Name;
            s.Address = se.Address;

            repository.Update(s);
            repository.Save();

            return true;
        }

        public object Delete(string id)
        {
            int _id = 0;

            if (!int.TryParse(id, out _id))
                return new ResponseError
                {
                    error_code = HttpStatusCode.BadRequest,
                    error_msg = "Bad Request"
                };

            var s = repository.FindBy(i => i.Id.Equals(_id)).SingleOrDefault();

            if (s == null)
                return new ResponseError
                {
                    error_code = HttpStatusCode.NotFound,
                    error_msg = "Record not found"
                };

            repository.Delete(s);
            repository.Save();

            return true;
        }
    }
}
