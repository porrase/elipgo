﻿using System.Windows.Forms;

namespace SuperZapatos.WinForm
{
    public partial class FrmLogIn : Form
    {
        public string Username
        {
            get
            {
                return txtUsername.Text;
            }
        }
        public FrmLogIn()
        {
            InitializeComponent();
        }       
    }
}
