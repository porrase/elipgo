﻿namespace SuperZapatos.WinForm
{
    partial class FrmArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnSave = new System.Windows.Forms.Button();
            this.BtnCancel = new System.Windows.Forms.Button();
            this.SplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lblStore = new System.Windows.Forms.Label();
            this.lblTotalVault = new System.Windows.Forms.Label();
            this.lblTotalSelf = new System.Windows.Forms.Label();
            this.lblPrice = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.CmbStore = new System.Windows.Forms.ComboBox();
            this.txtVoult = new System.Windows.Forms.TextBox();
            this.txtSelf = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).BeginInit();
            this.SplitContainer1.Panel1.SuspendLayout();
            this.SplitContainer1.Panel2.SuspendLayout();
            this.SplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnSave
            // 
            this.BtnSave.Location = new System.Drawing.Point(239, 154);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 23);
            this.BtnSave.TabIndex = 5;
            this.BtnSave.Text = "Guardar";
            this.BtnSave.UseVisualStyleBackColor = true;
            // 
            // BtnCancel
            // 
            this.BtnCancel.Location = new System.Drawing.Point(320, 154);
            this.BtnCancel.Name = "BtnCancel";
            this.BtnCancel.Size = new System.Drawing.Size(75, 23);
            this.BtnCancel.TabIndex = 6;
            this.BtnCancel.Text = "Cancelar";
            this.BtnCancel.UseVisualStyleBackColor = true;
            // 
            // SplitContainer1
            // 
            this.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer1.Name = "SplitContainer1";
            this.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainer1.Panel1
            // 
            this.SplitContainer1.Panel1.Controls.Add(this.lblStore);
            this.SplitContainer1.Panel1.Controls.Add(this.lblTotalVault);
            this.SplitContainer1.Panel1.Controls.Add(this.lblTotalSelf);
            this.SplitContainer1.Panel1.Controls.Add(this.lblPrice);
            this.SplitContainer1.Panel1.Controls.Add(this.lblName);
            this.SplitContainer1.Panel1.Controls.Add(this.CmbStore);
            this.SplitContainer1.Panel1.Controls.Add(this.txtVoult);
            this.SplitContainer1.Panel1.Controls.Add(this.txtSelf);
            this.SplitContainer1.Panel1.Controls.Add(this.txtPrice);
            this.SplitContainer1.Panel1.Controls.Add(this.txtName);
            this.SplitContainer1.Panel1.Controls.Add(this.BtnSave);
            this.SplitContainer1.Panel1.Controls.Add(this.BtnCancel);
            // 
            // SplitContainer1.Panel2
            // 
            this.SplitContainer1.Panel2.Controls.Add(this.dataGridView1);
            this.SplitContainer1.Size = new System.Drawing.Size(416, 450);
            this.SplitContainer1.SplitterDistance = 190;
            this.SplitContainer1.TabIndex = 13;
            // 
            // lblStore
            // 
            this.lblStore.AutoSize = true;
            this.lblStore.Location = new System.Drawing.Point(39, 121);
            this.lblStore.Name = "lblStore";
            this.lblStore.Size = new System.Drawing.Size(40, 13);
            this.lblStore.TabIndex = 22;
            this.lblStore.Text = "Tienda";
            // 
            // lblTotalVault
            // 
            this.lblTotalVault.AutoSize = true;
            this.lblTotalVault.Location = new System.Drawing.Point(25, 95);
            this.lblTotalVault.Name = "lblTotalVault";
            this.lblTotalVault.Size = new System.Drawing.Size(58, 13);
            this.lblTotalVault.TabIndex = 21;
            this.lblTotalVault.Text = "Valor Bulto";
            // 
            // lblTotalSelf
            // 
            this.lblTotalSelf.AutoSize = true;
            this.lblTotalSelf.Location = new System.Drawing.Point(15, 69);
            this.lblTotalSelf.Name = "lblTotalSelf";
            this.lblTotalSelf.Size = new System.Drawing.Size(70, 13);
            this.lblTotalSelf.TabIndex = 20;
            this.lblTotalSelf.Text = "Valor Unitario";
            // 
            // lblPrice
            // 
            this.lblPrice.AutoSize = true;
            this.lblPrice.Location = new System.Drawing.Point(46, 43);
            this.lblPrice.Name = "lblPrice";
            this.lblPrice.Size = new System.Drawing.Size(37, 13);
            this.lblPrice.TabIndex = 19;
            this.lblPrice.Text = "Precio";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(39, 17);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(44, 13);
            this.lblName.TabIndex = 18;
            this.lblName.Text = "Nombre";
            // 
            // CmbStore
            // 
            this.CmbStore.FormattingEnabled = true;
            this.CmbStore.Location = new System.Drawing.Point(91, 121);
            this.CmbStore.Name = "CmbStore";
            this.CmbStore.Size = new System.Drawing.Size(307, 21);
            this.CmbStore.TabIndex = 17;
            // 
            // txtVoult
            // 
            this.txtVoult.Location = new System.Drawing.Point(91, 95);
            this.txtVoult.Name = "txtVoult";
            this.txtVoult.Size = new System.Drawing.Size(100, 20);
            this.txtVoult.TabIndex = 16;
            // 
            // txtSelf
            // 
            this.txtSelf.Location = new System.Drawing.Point(91, 69);
            this.txtSelf.Name = "txtSelf";
            this.txtSelf.Size = new System.Drawing.Size(100, 20);
            this.txtSelf.TabIndex = 15;
            // 
            // txtPrice
            // 
            this.txtPrice.Location = new System.Drawing.Point(91, 43);
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(100, 20);
            this.txtPrice.TabIndex = 14;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(91, 17);
            this.txtName.MaxLength = 150;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(307, 20);
            this.txtName.TabIndex = 13;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(416, 256);
            this.dataGridView1.TabIndex = 8;
            // 
            // FrmArticle
            // 
            this.AcceptButton = this.BtnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.BtnCancel;
            this.ClientSize = new System.Drawing.Size(416, 450);
            this.Controls.Add(this.SplitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FrmArticle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Articulo";
            this.SplitContainer1.Panel1.ResumeLayout(false);
            this.SplitContainer1.Panel1.PerformLayout();
            this.SplitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).EndInit();
            this.SplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.Button BtnCancel;
        private System.Windows.Forms.SplitContainer SplitContainer1;
        private System.Windows.Forms.Label lblStore;
        private System.Windows.Forms.Label lblTotalVault;
        private System.Windows.Forms.Label lblTotalSelf;
        private System.Windows.Forms.Label lblPrice;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox CmbStore;
        private System.Windows.Forms.TextBox txtVoult;
        private System.Windows.Forms.TextBox txtSelf;
        private System.Windows.Forms.TextBox txtPrice;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}