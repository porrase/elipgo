﻿using System;
using System.Windows.Forms;
using SuperZapatos.Entity;

namespace SuperZapatos.WinForm
{
    public partial class FrmMain : Form
    {       
        #region Properties
        private UserEntity CurrentUser { get; set; }
        #endregion

        #region Contructor
        public FrmMain(UserEntity usr)
        {
            InitializeComponent();
            CurrentUser = usr;
        }

        public FrmMain()
        {
            InitializeComponent();
        }
        #endregion

        #region Events
        private void Main_Load(object sender, EventArgs e)
        {
            SetupMenu();
        }    

        private void MnuInventory_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmInventory());
        }

        private void MnuArticle_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmArticle());
        }

        private void MnuStore_Click(object sender, EventArgs e)
        {
            OpenForm(new FrmStore());
        }

        private void MnuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        #endregion       


        #region Methods

        private void SetupMenu()
        {
            if (!CurrentUser.UserPermission.AccessTo.Contains(PermissionEntity.EnumView.Administration))
            {
                mnuMain.Items["MnuAdmin"].Visible = false;
            }
        }

        private void OpenForm(Form frm)
        {
            if (Application.OpenForms[frm.Name] == null)
            {
                frm.MdiParent = this;
                frm.Show();
            }
            else
            {
                Application.OpenForms[frm.Name].Focus();
            }
        }


        //private async void AddProduct()
        //{           
        //    Product p = new Product();
        //    p.Id = 3;
        //    p.Name = "Rolex";
        //    p.Category = "Watch";
        //    p.Price = 1299936;
        //    using (var client = new HttpClient())
        //    {
        //        var serializedProduct = JsonConvert.SerializeObject(p);
        //        var content = new StringContent(serializedProduct, Encoding.UTF8, "application/json");
        //        var result = await client.PostAsync(URI, content);
        //    }
        //    GetAllProducts();                       
        //}

        //private async void UpdateProduct()
        //{
        //    Product p = new Product();
        //    p.Id = 3;
        //    p.Name = "Rolex";
        //    p.Category = "Watch";
        //    p.Price = 1400000; //changed the price

        //    using (var client = new HttpClient())
        //    {
        //        var serializedProduct = JsonConvert.SerializeObject(p);
        //        var content = new StringContent(serializedProduct, Encoding.UTF8, "application/json");
        //        var result = await client.PutAsync(String.Format("{0}/{1}", URI, p.Id), content);
        //    }
        //    GetAllProducts(); 
        //}

        //private async void DeleteProduct()
        //{
        //    using (var client = new HttpClient())
        //    {                
        //        var result = await client.DeleteAsync(String.Format("{0}/{1}", URI, 3));
        //    }            
        //    GetAllProducts();
        //}



        #endregion

    
    }
}
