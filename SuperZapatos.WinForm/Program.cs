﻿using System;
using System.Windows.Forms;

namespace SuperZapatos.WinForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            FrmLogIn fLogin = new FrmLogIn();
            if (fLogin.ShowDialog() == DialogResult.OK)
            {               
                Application.Run(new FrmMain(new Entity.UserEntity() { Username = fLogin.Username }));
            }
            else
            {
                Application.Exit();
            }            
        }
    }
}
