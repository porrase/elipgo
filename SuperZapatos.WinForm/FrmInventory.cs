﻿using SuperZapatos.Entity;
using SuperZapatos.Entity.API;
using System;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SuperZapatos.WinForm
{
    public partial class FrmInventory : Form
    {
        private BindingSource bindingSource = new BindingSource();

        public FrmInventory()
        {
            InitializeComponent();
        }

        #region Events
        private void FrmInventory_Load(object sender, System.EventArgs e)
        {
            BindAsync();
        }

       
        private  void button1_Click(object sender, System.EventArgs e)
        {
            BuscarAsync();
        }
        #endregion

        #region methods
        private  async void BindAsync()
        {
            try
            {           
                StoreResponse dataStores = await Services.StoreService.GetAllAsync();            

                if (dataStores.success )
                {
                    dataStores.stores.Insert(0, new StoreEntity { Id = -1, Name = "Todas" });
                    bindingSource.DataSource = dataStores.stores;

                    CmbStore.DataSource = bindingSource.DataSource;
                    CmbStore.DisplayMember = "Name";
                    CmbStore.ValueMember = "Id";
                }
            }
            catch (Exception)
            {
                //TODO:Log errores
                MessageBox.Show("No se cargaron las Tiendas.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }
        }

        private async void BuscarAsync()
        {
            try
            {           
                ArticleResponse dataArticles = null;

                if (CmbStore.SelectedValue != null)
                {
                    if ((int)CmbStore.SelectedValue == -1)
                        dataArticles = await Services.ArticleService.GetAllAsync();
                    else
                        dataArticles = await Services.ArticleService.GetByStoreAsync((int)CmbStore.SelectedValue);

                    if (dataArticles.success)
                    {
                        bindingSource.DataSource = dataArticles.articles;
                        DgInventory.DataSource = bindingSource.DataSource;
                    }
                    else
                    {
                        //TODO:Log errores
                        MessageBox.Show("No se cargaron los Articulos.", "Error",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception)
            {
                //TODO:Log errores
                MessageBox.Show("No se cargaron los Articulos.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion
    }
}
