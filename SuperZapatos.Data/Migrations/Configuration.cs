﻿using SuperZapatos.Data.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Data.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            bool runSeed = true;
            if (runSeed)
            {
                AddStore(context);
                AddArticles(context);
            }
        }

        private void AddStore(ApplicationDbContext context)
        {
            context.Stores.AddOrUpdate(x => x.Id,
                new Model.Store() { Id = 1, Name = "Store 1", Address = "Direcion Store 1" },
                new Model.Store() { Id = 2, Name = "Store 2", Address = "Direcion Store 2" },
                new Model.Store() { Id = 3, Name = "Store 3", Address = "Direcion Store 3" },
                new Model.Store() { Id = 4, Name = "Store 4", Address = "Direcion Store 4" },
                new Model.Store() { Id = 5, Name = "Store 5", Address = "Direcion Store 5" }
            );

            context.SaveChanges();
        }

        private void AddArticles(ApplicationDbContext context)
        {
            context.Articles.AddOrUpdate(x => x.Id,
                new Model.Article() { Id = 1, Name = "Zapatilla nro 1", Price = Convert.ToDecimal("20,56"), total_in_shelf = Convert.ToDecimal("10.04"), total_in_vault = Convert.ToDecimal("44.10"), StoreId = 2 },
                new Model.Article() { Id = 2, Name = "Zapatilla nro 2", Price = Convert.ToDecimal("20,56"), total_in_shelf = Convert.ToDecimal("10.04"), total_in_vault = Convert.ToDecimal("44.10"), StoreId = 1 },
                new Model.Article() { Id = 3, Name = "Zapatilla nro 3", Price = Convert.ToDecimal("20,56"), total_in_shelf = Convert.ToDecimal("10.04"), total_in_vault = Convert.ToDecimal("44.10"), StoreId = 1 },
                new Model.Article() { Id = 4, Name = "Zapatilla nro 4", Price = Convert.ToDecimal("20,56"), total_in_shelf = Convert.ToDecimal("10.04"), total_in_vault = Convert.ToDecimal("44.10"), StoreId = 3 },
                new Model.Article() { Id = 5, Name = "Zapatilla nro 5", Price = Convert.ToDecimal("20,56"), total_in_shelf = Convert.ToDecimal("10.04"), total_in_vault = Convert.ToDecimal("44.10"), StoreId = 3 },
                new Model.Article() { Id = 6, Name = "Zapatilla nro 6", Price = Convert.ToDecimal("20,56"), total_in_shelf = Convert.ToDecimal("10.04"), total_in_vault = Convert.ToDecimal("44.10"), StoreId = 3 },
                new Model.Article() { Id = 7, Name = "Zapatilla nro 7", Price = Convert.ToDecimal("20,56"), total_in_shelf = Convert.ToDecimal("10.04"), total_in_vault = Convert.ToDecimal("44.10"), StoreId = 3 },
                new Model.Article() { Id = 8, Name = "Zapatilla nro 8", Price = Convert.ToDecimal("20,56"), total_in_shelf = Convert.ToDecimal("10.04"), total_in_vault = Convert.ToDecimal("44.10"), StoreId = 2 },
                new Model.Article() { Id = 9, Name = "Zapatilla nro 9", Price = Convert.ToDecimal("20,56"), total_in_shelf = Convert.ToDecimal("10.04"), total_in_vault = Convert.ToDecimal("44.10"), StoreId = 2 },
                new Model.Article() { Id = 10, Name = "Zapatilla nro 10", Price = Convert.ToDecimal("20,56"), total_in_shelf = Convert.ToDecimal("10.04"), total_in_vault = Convert.ToDecimal("44.10"), StoreId = 3 }
            );

            context.SaveChanges();

        }
    }
}