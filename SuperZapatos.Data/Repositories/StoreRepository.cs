﻿using SuperZapato.Entity;
using SuperZapatos.Data.Model;
using SuperZapatos.Data.Repositories.Base;
using SuperZapatos.Entity;
using System.Linq;

namespace SuperZapatos.Data.Repositories
{
    public class StoreRepository : BaseRepository<ApplicationDbContext, Store>
    {
        public IQueryable<StoreEntity> GetAll()
        {
            return base.GetAll().Select(c => new StoreEntity
            {
                Id = c.Id,
                Address = c.Address,
                Name = c.Name
            });

        }
    }
}
