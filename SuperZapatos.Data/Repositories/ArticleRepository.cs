﻿using SuperZapato.Entity;
using SuperZapatos.Data.Model;
using SuperZapatos.Data.Repositories.Base;
using SuperZapatos.Entity;
using System.Linq;

namespace SuperZapatos.Data.Repositories
{
    public class ArticleRepository : BaseRepository<ApplicationDbContext, Article>
    {
        public IQueryable<ArticleEntity> GetAll()
        {
            return base.GetAll().Select(c => new ArticleEntity
            {
                Id = c.Id,
                Name = c.Name,
                Price = c.Price,
                Total_In_Shelf = c.total_in_shelf,
                Total_In_Vault = c.total_in_vault,
                Store_Name = c.Store.Name
            });
        }

        public IQueryable<ArticleEntity> GetArticleById(int id)
        {
            return base.FindBy(c => c.Store.Id.Equals(id)).Select(a =>
               new ArticleEntity
               {
                   Id = a.Id,
                   Name = a.Name,
                   Price = a.Price,
                   Total_In_Shelf = a.total_in_shelf,
                   Total_In_Vault = a.total_in_vault,
                   Store_Name = a.Store.Name
               });
        }
    }
}
