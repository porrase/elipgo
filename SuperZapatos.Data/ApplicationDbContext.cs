﻿using SuperZapatos.Data.Model;
using System.Configuration;
using System.Data.Entity;

namespace SuperZapatos.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Article> Articles { get; set; }
        public DbSet<Store> Stores { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
