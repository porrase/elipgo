﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Entity.API
{
    public class ResponseOk
    {
        public bool success { get; set; } = true;
        public int total_elements { get; set; }
    }
}
