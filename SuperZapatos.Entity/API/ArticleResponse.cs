﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Entity.API
{
    public class ArticleResponse : ResponseOk
    {
        public List<ArticleEntity> articles { get; set; }
    }
}
