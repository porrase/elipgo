﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapato.Entity.API
{
    public class ResponseError
    {
        public string error_msg { get; set; }
        public HttpStatusCode error_code { get; set; }
        public bool success { get; set; } = false;
    }
}
