﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Entity
{
    public class PermissionEntity
    {
        public PermissionEntity()
        {
            AccessTo = new List<EnumView>();
        }
        public enum EnumView : int
        {
            Inventory = 0,
            Administration = 1
        }

        public List<EnumView> AccessTo { get; set; }
    }
}
