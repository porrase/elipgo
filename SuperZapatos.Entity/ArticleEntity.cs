﻿using System;

namespace SuperZapatos.Entity
{
    public class ArticleEntity
    {
        public Int32 Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Total_In_Shelf { get; set; }
        public decimal Total_In_Vault { get; set; }
        public string Store_Name { get; set; }
    }
}
