﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Entity
{
    public class UserEntity
    {
        public string Username { get; set; }

        public PermissionEntity UserPermission
        {
            get
            {
                PermissionEntity result = new PermissionEntity();
                if( Username.ToUpper().Equals("ADMIN") )
                {
                    result.AccessTo.Add(PermissionEntity.EnumView.Administration);
                    result.AccessTo.Add(PermissionEntity.EnumView.Inventory);
                }
                else
                    result.AccessTo.Add(PermissionEntity.EnumView.Inventory);

                return result;
            }            
        }      

            

    }
}
