﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Services.Helper
{
    public abstract class AppSettings
    {
        public static string ApiUrl = ConfigurationManager.AppSettings["apiUrl"].ToString();
    }
}
