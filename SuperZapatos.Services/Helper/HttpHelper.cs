﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Services.Helper
{
    public class HttpHelper
    {
        public static async Task<string> DoGetAsync(string url)
        {
            string resultado = string.Empty;

            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync(url))
                {
                    if (response.IsSuccessStatusCode)
                    {
                        resultado = await response.Content.ReadAsStringAsync();                        
                    }
                }
            }

            return resultado;
        }
    }
}
