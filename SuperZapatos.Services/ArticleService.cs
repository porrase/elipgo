﻿using SuperZapatos.Entity.API;
using SuperZapatos.Services.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Services
{
    public class ArticleService
    {

        public static async Task<ArticleResponse> GetAllAsync()
        {
            ArticleResponse resultado = new ArticleResponse();

            var storeJsonString = await HttpHelper.DoGetAsync($"{AppSettings.ApiUrl}/services/articles");

            Newtonsoft.Json.JsonConvert.PopulateObject(storeJsonString, resultado);

            return resultado;
        }

        public static async Task<ArticleResponse> GetByStoreAsync(int id)
        {
            ArticleResponse resultado = new ArticleResponse();

            var storeJsonString = await HttpHelper.DoGetAsync($"{AppSettings.ApiUrl}/services/articles/stores/{id}");

            Newtonsoft.Json.JsonConvert.PopulateObject(storeJsonString, resultado);

            return resultado;           
        }
    }
}
