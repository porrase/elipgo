﻿using SuperZapatos.Entity.API;
using SuperZapatos.Services.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace SuperZapatos.Services
{
    public class StoreService
    {
        public static async Task<StoreResponse> GetAllAsync()
        {
            StoreResponse resultado = new StoreResponse();

            var storeJsonString = await HttpHelper.DoGetAsync($"{AppSettings.ApiUrl}/services/stores");

            Newtonsoft.Json.JsonConvert.PopulateObject(storeJsonString, resultado);

            return resultado;
        }       
    }
}
